OBJS	= n0nc.o
OUTPUT	= n0nc

CC	= gcc
CFLAGS	= -Wall -Wextra
LFLAGS	=
DFLAGS	=

BUILD	= $(shell git log -1 --pretty=format:%h)

ifdef OS
    # We're on Windows
    LFLAGS += -lws2_32
endif

ifdef SPANISH
    DFLAGS += -DN0NC_SPANISH
endif

ifdef UPNP
    DFLAGS += -DN0NC_UPNP
    LFLAGS += -lminiupnpc
endif

ifdef DEBUG
    CFLAGS += -g
    BUILD := $(BUILD)+debug
else
    CFLAGS += -O2
endif


all: build.h $(OBJS)
	@echo "CC $(OUTPUT)"
	@$(CC) $(CFLAGS) $(OBJS) -o $(OUTPUT) $(LFLAGS)

%.o: %.c
	@echo "CC $@"
	@$(CC) $(CFLAGS) $(DFLAGS) $(IFLAGS) -c $< -o $@

.PHONY: build.h
build.h:
	@echo "BUILD $(BUILD)"
	@echo '#define BUILD "<$(BUILD)>"' > build.h

clean:
	@echo 'RM $(OUTPUT) $(OBJS) build.h'
	@rm -f $(OUTPUT) $(OBJS) build.h

