# N0NC

I used to exchange files between computers using netcat, however, this was awful and a pain to do, so i coded N0NC to make things easier to me.  The two computers are owned by my friends, one Linux box and one Windows, so i coded with the idea to make it work for both platforms, and since other friend of mine wanted the program in Spanish, we translated it.

N0NC is a very simple program, you first have to serve the file(s) from a computer, the program will bind itself to a port between 1024 and 65535, then you open N0NC with the ip address of the serving computer and the port it bounded to, then execute it and the file sharing will begin.  

## Usage

To serve a file, you have to use the `send` command with the file you want to serve, if you want to serve several files, just add them too:

Note: Everything is exactly the same in Windows as in Linux, but for Windows use the appropriate directory separator `\` instead of `/`.

```
$ n0nc send file1 file2 dir/file5
```

The program will show its version (the version on both computer must match!)
and the port it's bounded to, assuming that its ip is `192.168.1.2` and the bounded port was `1234`, you use this command on the destination computer.

```
$ n0nc recv 192.168.1.2:1234
```

Then both programs will begin the transmission of files. When the transmission is finished, the server will keep running, just send Ctrl-C to finish it, or if you want deliver the same files to any other computer, just connect again to the server.

If you want to share a file outside your local network, it's very likely that you receiver won't connect, that's because you router blocks external connections, if you has built with UPNP support, you can use `send+`, that will make a redirection in you router allowing sharing files through different networks.

Note: You need a router with UPNP enabled and use `send+`, check that out.

## Building

To make a simple build, just use `make` in the repository. To compile with debug information just add `DEBUG=1`, to compile with Spanish translations add `SPANISH=1`, to compile with support for UPNP add `UPNP=1`.

```
$ make                          # simple build
$ make DEBUG=1                  # build with debug ('-g')
$ make UPNP=1                   # build with UPNP
$ make SPANISH=1 UPNP=1         # build with UPNP and Spanish translations
$ make DEBUG=1 SPANISH=1 UPNP=1 # build with everything
```

## TODO List

* Encryption: connections are unencrypted, adding it to non-local (between two computers in the same network) will make stuff safer.
* Compression: wouldn't be nice?
* Integrity checks: using checksum or some hash to check packages.

