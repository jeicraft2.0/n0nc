/* n0nc - simple file sharing between two computers
 *
 * Copyright (C) 2018 Neutral0 <zarwarezw@gmail.com>,
 *                    Jeicraft <jeicraft2.0@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef __WIN32__
#   include <sys/types.h>
#   include <sys/socket.h>
#   include <sys/select.h>
#   include <sys/stat.h>
#   include <netinet/in.h>
#   include <arpa/inet.h>
#else
#   define WIN32_LEAN_AND_MEAN
#   include <windows.h>
#   include <winsock2.h>
#   include <ws2tcpip.h>
#endif

#ifdef N0NC_UPNP
#   include <miniupnpc/miniupnpc.h>
#   include <miniupnpc/upnpcommands.h>
#endif

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#ifndef __WIN32__
#   define FFLUSH(f) fflush(f)
#else
#   define FFLUSH(f)
#endif

#define VERSION "0.1.7"
#define PATCH "d"
#define EXTENSION ""
#define FILESHARE "FILESHARE " VERSION

#include "build.h"

#ifdef N0NC_UPNP
#   undef EXTENSION
#   define EXTENSION ", with UPNP"
#endif

#ifdef N0NC_SPANISH

#pragma GCC diagnostic ignored "-Wformat-security"

struct translation {
    const char  *orig;
    const char  *es;
};

static struct translation messagesTable[] = {
    {"%s: cannot stat file: %s",    "%s: no se puede analizar archivo: %s"},
    {"%s: invalid file type",       "%s: tipo de archivo invalido"},
    {"%s: cannot seek file: %s",    "%s: no se puede medir el archivo: %s"},
    {"Handshake... ",               "Sincronizando... "},
    {"error waiting",               "error de espera"},
    {"polling error: %s",           "error polling: %s"},
    {"connection error",            "error de conexion"},
    {"error reading: %s",           "error leyendo: %s"},
    {"connection closed",           "conexion cerrada"},
    {"client aborted",              "cliete abortado"},
    {"invalid light",               "luz invalida"},
    {"timeout",                     "tiempo de espera agotado"},
    {"\r\t\t\t\rSending '%s'... ",  "\r\t\t\t\rEnviando '%s'... "},
    {"cannot open file '%s': %s",   "no se puede abrir el archivo '%s': %s"},
    {"\bSuccess",                   "\bExito"},
    {"\bError",                     "\bError"},
    {"connection finished.",        "conexion finalizada."},
    {"Transference complete.",      "Transferencia completada."},
    {"cannot create socket: %s",    "no se puede crear socket: %s"},
    {"listening at port %hu\n",     "escuchando en el puerto %hu\n"},
    {"cannot bind socket: %s",      "no se puede enlazar socket: %s"},
    {"cannot listen socket: %s",    "no se puede eschuchar socket: %s"},
    {"Connection from %s",          "Conexion desde %s"},
    {" cancelled",                  " cancelado"},
    {" invalid handshake",          " sincronizacion invalida"},
    {" begins.",                    " comienza."},
    {"missing port.",               "puerto faltante."},
    {"invalid host: %s",            "host invalido. %s"},
    {"invalid port: %s",            "puerto invalid: %s"},
    {"invalid port range.",         "range de puerto invalido."},
    {"connecting...",               "conectando..."},
    {" failed",                     " fallido"},
    {"cannot connect: %s",          "no se puede conectar: %s"},
    {"invalid responce from server.", "respuesta invalida del servidor."},
    {" done",                       " hecho"},
    {"missing arguments.",          "faltan argumentos."},
    {"invalid mode '%s'",           "modo invalido '%s'"},
#ifdef N0NC_UPNP
    {"no UPNP device found: %i",    "no se encuentra dispositivo UPNP: %i"},
    {"unsure about IGD '%s': %i",   "inseguro acerca el IGD '%s': %i"},
    {"continue anyway.",            "continuando de todas formas"},
    {"cannot map port: %i",         "no sep puede mapear puerto: %i"},
    {"Port %hu of %s is mapped to %s.\n", "Puerto %hu de %s esta mapeado a %s.\n"},
    {"UPNP_DeletePortMapping() failed with code: %i", "UPNP_DeletePortMapping() fallo con codigo: %i"},
    {"Creating UPNP port mapping... ", "Creando mapeo de puerto UPNP... "},
    {"Cleaning UPNP port mapping.", "Limpiando mapeo de puerto UPNP."},
#endif
    {"UPNP unsupported",            "UPNP no soportado"},
    {"host:port required",          "host:puerto requerido"}
};

#define _(msg) translate(msg)

static int
_sort(const void *a, const void *b)
{
    const struct translation *t1 = (const struct translation*)a;
    const struct translation *t2 = (const struct translation*)b;

    return strcmp(t1->orig, t2->orig);
}

static int
_compar(const void *a, const void *b)
{
    const struct translation *t = (const struct translation*)b;

    return strcmp((const char*)a, t->orig);
}

static inline const char *
translate(const char *msg)
{
    struct translation *t;

    t = bsearch(msg, &messagesTable,
                sizeof(messagesTable) / sizeof(struct translation),
                sizeof(struct translation), _compar);

    return (t)? t->es : msg;
}

#else

#define _(msg) msg

#endif

static const char   *arg0;

#if defined(__WIN32__)

static int
inet_pton(int af, const char *src, void *dst)
{
    struct sockaddr_storage ss;
    int size = sizeof(ss);
    char src_copy[INET6_ADDRSTRLEN+1];

    ZeroMemory(&ss, sizeof(ss));
    strncpy (src_copy, src, INET6_ADDRSTRLEN+1);
    src_copy[INET6_ADDRSTRLEN] = 0;

    if (WSAStringToAddress(src_copy, af, NULL,
                           (struct sockaddr*)&ss, &size) == 0) {
        switch(af) {
            case AF_INET:
                *(struct in_addr *)dst = ((struct sockaddr_in *)&ss)->sin_addr;
                return 1;
        }
    }

    return 0;
}

static const char *
inet_ntop(int af, const void *src, char *dst, socklen_t size)
{
  struct sockaddr_storage ss;
  unsigned long s = size;

  ZeroMemory(&ss, sizeof(ss));
  ss.ss_family = af;

  switch(af) {
    case AF_INET:
      ((struct sockaddr_in *)&ss)->sin_addr = *(struct in_addr *)src;
      break;
    default:
      return NULL;
  }

  return (WSAAddressToString((struct sockaddr *)&ss,
                             sizeof(ss), NULL, dst, &s) == 0)? dst : NULL;
}

#endif

static inline void
err(int e, const char *fmt, ...)
{
    va_list vl;

    va_start(vl, fmt);

    fprintf(stderr, "%s: ", arg0);
    vfprintf(stderr, fmt, vl);
    fputc('\n', stderr);

    va_end(vl);

    if(e) exit(e);
}

#ifdef N0NC_UPNP

static struct UPNPDev *devlist;
static struct UPNPUrls urls;
static struct IGDdatas data;
static unsigned short portOpened;

static void
openPort(unsigned short port)
{
    char lanaddr[64] = "(lan)";
    char wanaddr[64] = "(wan)";
    char portStr[6];
    int error, i;

    devlist     = NULL;
    portOpened  = 0;

    /* Longest string "65535" fit perfectly */
    sprintf(portStr, "%hu", port);

    printf("1");
    FFLUSH(stdout);

    devlist = upnpDiscover(2000, NULL, NULL, UPNP_LOCAL_PORT_ANY,
                           0, 2, &error);
    if(!devlist) {
        puts(_("\bError"));
        err(0, _("no UPNP device found: %i"), error);
        return ;
    }

    printf("\b2");
    FFLUSH(stdout);

    i = UPNP_GetValidIGD(devlist, &urls, &data, lanaddr, sizeof(lanaddr));
    if(i != 1) {
        err(0, _("unsure about IGD '%s': %i"), urls.controlURL, i);
        err(0, _("continue anyway."));
    }

    UPNP_GetExternalIPAddress(urls.controlURL, data.first.servicetype,
                                  wanaddr);

    i = UPNP_AddPortMapping(urls.controlURL, data.first.servicetype,
                            portStr, portStr, lanaddr, "n0nc", "TCP", 0, "0");
    if(i != UPNPCOMMAND_SUCCESS) {
        puts(_("\bError"));
        err(0, _("cannot map port: %i"), i);
        FreeUPNPUrls(&urls);
        freeUPNPDevlist(devlist);
        return ;
    }

    portOpened = port;

    puts("\bSuccess");
    printf(_("Port %hu of %s is mapped to %s.\n"), port, wanaddr, lanaddr);
}

static void
closePort(void)
{
    char portStr[6];
    int r;

    if(!portOpened)
        return ;

    sprintf(portStr, "%hu", portOpened);

    r = UPNP_DeletePortMapping(urls.controlURL, data.first.servicetype,
                               portStr, "TCP", NULL);
    if(r != UPNPCOMMAND_SUCCESS)
        err(0, _("UPNP_DeletePortMapping() failed with code: %i"), r);

    FreeUPNPUrls(&urls);
    freeUPNPDevlist(devlist);
}

#endif

static int
waitSocket(int sfd, int timeout)
{
    fd_set fds;
    struct timeval tv;

    FD_ZERO(&fds);
    FD_SET(sfd, &fds);

    tv.tv_sec   = (timeout / 1000);
    tv.tv_usec  = (timeout % 1000) * 1000;

    return select(sfd+1, &fds, NULL, NULL, &tv);
}

static unsigned
getTime(void)
{
    unsigned int ms;

#ifndef __WIN32__
    struct timespec ts;

    clock_gettime(CLOCK_MONOTONIC, &ts);

    ms  = ts.tv_sec  * 1000;
    ms += ts.tv_nsec / 1000000;
#else
    SYSTEMTIME st;

    GetSystemTime(&st);

    ms  = st.wMinute * 60000 + st.wSecond * 1000;
    ms += st.wMilliseconds;
#endif

    return ms;
}

static void
progress(const char *fname, size_t size, size_t totalSize)
{
    char progbar[80];
    unsigned prog, spac;
    double perc;

    perc = (double)size / (double)totalSize;
    prog = 21 * perc;
    spac = 21 - prog;

    progbar[0] = '\r';
    progbar[1] = '[';

    memset(progbar+2, '=', prog);
    memset(progbar+2+prog, ' ', spac);

    progbar[2+prog] = '>';
    progbar[23] = ']';
    progbar[24] = ' ';

    sprintf(progbar+25, "%5.1f%% ", perc * 100.0);

    snprintf(progbar+32, 48, "%s", fname);

    fputs(progbar, stdout);
    FFLUSH(stdout);
}

static inline const char *
basename(const char *path)
{
    const char *head;

#ifndef __WIN32__
    head = strrchr(path, '/');
#else
    head = strrchr(path, '\\');
#endif

    return (head)? head+1 : path;
}

static int
sendFileHeader(int sfd, FILE *src, const char *name)
{
    char header[4096], buf[512];
    int length, ret, i;
    unsigned long fsize;

#ifndef __WIN32__
    struct stat sb;
    int ffd;

    ffd = fileno(src);

    if(fstat(ffd, &sb) < 0) {
        err(0, _("%s: cannot stat file: %s"), name, strerror(errno));
        return -1;
    }

    if((sb.st_mode & S_IFMT) != S_IFREG) {
        err(0, _("%s: invalid file type"), name);
        return -1;
    }

    fsize = sb.st_size;
#else
    if(fseek(src, 0, SEEK_END) < 0) {
        err(0, _("%s: cannot seek file: %s"), name, strerror(errno));
        return -1;
    }

    fsize = ftell(src);

    fseek(src, 0, SEEK_SET);
#endif

    printf(_("Handshake... "));
    FFLUSH(stdout);

    length = snprintf(header, 4096, "%lu %s", fsize, basename(name)) + 1;

    for(i = 0; i < 5; ++i) {
        ret = waitSocket(sfd, 5000);
        if(ret < 0) {
            puts(_("error waiting"));
            err(0, _("polling error: %s"), strerror(errno));
            return -1;
        } else if(ret == 0) {
            putchar('*');
            FFLUSH(stdout);
            continue;
        }

        ret = recv(sfd, buf, 512, 0);
        if(ret <= 0) {
            puts(_("connection error"));
            if(ret < 0)
                err(0, _("error reading: %s"), strerror(errno));
            else
                puts(_("connection closed"));
            return -1;
        }

        if(strcmp(buf, "YELLOWLIGHT") == 0) {
            send(sfd, header, length, 0);
            break;
        }

        if(strcmp(buf, "REDLIGHT") == 0) {
            puts(_("client aborted"));
            return -1;
        }

        puts(_("invalid light"));
        return -1;
    }

    if(i == 5) {
        puts(_("timeout"));
        return -1;
    }

    for(i = 0; i < 5; ++i) {
        ret = waitSocket(sfd, 5000);
        if(ret < 0) {
            puts(_("error waiting"));
            err(0, _("polling error: %s"), strerror(errno));
            return -1;
        } else if(ret == 0) {
            putchar('+');
            FFLUSH(stdout);
            continue;
        }

        ret = recv(sfd, buf, 512, 0);
        if(ret <= 0) {
            puts(_("connection error"));
            if(ret < 0)
                err(0, _("error reading: %s"), strerror(errno));
            else
                puts(_("connection closed"));
            return -1;
        }

        if(strcmp(buf, "GREENLIGHT") == 0)
            break;

        if(strcmp(buf, "REDLIGHT") == 0) {
            puts(_("client aborted"));
            return -1;
        }

        puts(_("invalid light"));
        return -1;
    }

    printf(_("\r\t\t\t\r" /* Clears "handshake..." */
             "Sending '%s'... "), basename(name));
    FFLUSH(stdout);

    return 0;
}

static void
sendLoop(int sfd, char *files[])
{
    char **curFile;
    char buffer[4096];
    size_t ret, cnt;
    FILE *src;
    unsigned t1, t2;

    curFile = files;

    while(*curFile) {
        src = fopen(*curFile, "rb");
        if(!src) {
            err(0, _("cannot open file '%s': %s"), *curFile, strerror(errno));
            fclose(src);
            ++curFile;
            continue;
        }

        if(sendFileHeader(sfd, src, *curFile) < 0) {
            ++curFile;
            continue;
        }

        t1 = getTime() + 100;

        cnt = 0;
        for(;;) {
            ret = fread(buffer, 1, 4096, src);
            if(ret == 0) {
                puts(_("\bSuccess"));
                break;
            }

            if(send(sfd, buffer, ret, 0) < 0) {
                puts(_("\bError"));
                break;
            }

            t2 = getTime();

            if(t2 >= t1) {
                putchar('\b');
                putchar("|/-\\|/-\\"[cnt++ & 7]);
                FFLUSH(stdout);

                t1 = t2 + 100;
            }
        }

        fclose(src);
        ++curFile;
    }

    close(sfd);
}

static int
recvFileHeader(int sfd, size_t *sizep, char **fnamep)
{
    char buffer[4096];
    char *ptr;
    int i, ret;

    printf(_("Handshake... "));
    FFLUSH(stdout);

    for(i = 0; i < 5; ++i) {
        send(sfd, "YELLOWLIGHT", 13, 0);

        ret = waitSocket(sfd, 5000);
        if(ret < 0) {
            puts(_("error waiting"));
            err(0, _("polling error: %s"), strerror(errno));
            return 0;
        } else if(ret == 0) {
            putchar('*');
            FFLUSH(stdout);
            continue;
        }

        ret = recv(sfd, buffer, 4096, 0);
        if(ret <= 0) {
            printf("\r\t\t\t\r");
            FFLUSH(stdout);

            if(ret < 0)
                err(0, _("error reading: %s"), strerror(errno));

            puts(_("connection finished."));
            return 0;
        }

        if(strcmp(buffer, "REDLIGHT") == 0)
            return 0;

        /* Assumed server never sends ill-formed file header */
        ptr = strchr(buffer, ' ');
        *ptr++ = '\0';

        *sizep = strtoul(buffer, NULL, 10);
        *fnamep = strdup(ptr);
        break;
    }

    if(i == 5) {
        puts(_("timeout"));
        return 0;
    }

    send(sfd, "GREENLIGHT", 12, 0);

    return 1;
}

static void
recvLoop(int sfd)
{
    char *curFile;
    char buffer[4096];
    size_t ret, size, written;
    FILE *dst;
    unsigned t1, t2;

    while(recvFileHeader(sfd, &size, &curFile)) {
        dst = fopen(curFile, "wb");
        if(!dst) {
            err(0, _("cannot open file '%s': %s"), curFile, strerror(errno));
            free(curFile);
            send(sfd, "REDLIGHT", 10, 0);
            continue;
        }

        t1 = getTime() + 100;

        progress(curFile, 0, size);

        written = 0;
        while(written < size) {
            t2 = getTime();
            if(t2 >= t1) {
                progress(curFile, written, size);
                t1 = t2 + 100;
            }

            ret = recv(sfd, buffer, 4096, 0);
            if(ret <= 0) {
                /* TODO */
                break;
            }

            fwrite(buffer, 1, ret, dst);

            written += ret;
        }

        progress(curFile, written, size);
        putchar('\n');

        fclose(dst);
        free(curFile);
    }

    puts(_("Transference complete."));

    close(sfd);
}

static void
sendMode(char *files[], int upnp)
{
    struct sockaddr_in sin;
    char addrbuf[256];
    unsigned short port;
    size_t addrlen;
    int sfd, cfd;

#ifndef N0NC_UPNP
    if(upnp)
        err(0, _("UPNP unsupported"));
#endif

    sfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sfd < 0)
        err(1, _("cannot create socket: %s"), strerror(errno));

    port = ((unsigned)(time(NULL) * 347993) % 64511) + 1024;
    printf(_("listening at port %hu\n"), port);

    sin.sin_family      = AF_INET;
    sin.sin_port        = htons(port);
    sin.sin_addr.s_addr = INADDR_ANY;

    if(bind(sfd, (struct sockaddr*)&sin, sizeof(sin)) < 0)
        err(1, _("cannot bind socket: %s"), strerror(errno));

    if(listen(sfd, 128) < 0)
        err(1, _("cannot listen socket: %s"), strerror(errno));

#ifdef N0NC_UPNP
    if(upnp) {
        printf(_("Creating UPNP port mapping... "));
        FFLUSH(stdout);
        openPort(port);
    }
#endif

    addrlen = sizeof(sin);
    if((cfd = accept(sfd,
                    (struct sockaddr*)&sin,
                    (socklen_t*)&addrlen)) != -1) {
        char header[512];
        ssize_t ret;

        inet_ntop(AF_INET, &sin, addrbuf, 256);

        printf(_("Connection from %s"), addrbuf);
        FFLUSH(stdout);

        ret = recv(cfd, (char*)&header, 512, 0);
        if(ret <= 0) {
            puts(_(" cancelled"));
            close(cfd);
            return;
        }

        if(strcmp(header, FILESHARE) != 0) {
            puts(_(" invalid handshake"));
            close(cfd);
            return;
        }

        send(cfd, FILESHARE, sizeof(FILESHARE) + 1, 0);

        puts(_(" begins."));
        sendLoop(cfd, files);

        puts(_("connection finished."));
    }

#ifdef N0NC_UPNP
    if(upnp) {
        puts("Cleaning UPNP port mapping.");
        closePort();
    }
#endif
}

static void
recvMode(char *addr)
{
    char *host, *port;
    struct sockaddr_in sin;
    unsigned long portVal;
    char header[512];
    char *end;
    int sfd, ret;

    host = addr;
    port = strchr(addr, ':');

    if(!port)
        err(1, _("missing port."));

    *port++ = '\0';

    if(strcmp(host, "localhost") == 0)
        sin.sin_addr.s_addr = INADDR_ANY;
    else if(inet_pton(AF_INET, host, &sin.sin_addr) < 0)
        err(1, _("invalid host: %s"), strerror(errno));

    portVal = strtoul(port, &end, 10);
    if(*end)            err(1, _("invalid port: %s"), strerror(errno));
    if(portVal > 65565) err(1, _("invalid port range."));

    sin.sin_family  = AF_INET;
    sin.sin_port    = htons(portVal);

    sfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sfd < 0)
        err(1, _("cannot create socket: %s"), strerror(errno));

    printf(_("connecting..."));

    FFLUSH(stdout);

    ret = connect(sfd, (struct sockaddr*)&sin, sizeof(sin));
    if(ret < 0) {
        puts(_(" failed"));
        err(1, _("cannot connect: %s"), strerror(errno));
    }

    send(sfd, FILESHARE, sizeof(FILESHARE), 0);

    ret = recv(sfd, (char*)&header, 512, 0);
    if(ret <= 0) {
        puts(_(" failed"));
        err(1, _("invalid responce from server."));
    }

    if(strcmp(header, FILESHARE) != 0) {
        puts(_(" failed"));
        err(1, _("invalid responce from server."));
    }

    puts(_(" done"));

    recvLoop(sfd);
}

static void
showHelp(void)
{
#ifndef N0NC_SPANISH
    fprintf(stderr,
        "Usage: %s [-s | send] Files...\n"
        "       %s [-r | recv] host:port\n\n"
        "Options:\n"
        "    -s send    Start a server and send files.\n"
        "    -r recv    Connect to a server and receive files.\n"
        "    -h         Show this message.\n",
        arg0, arg0);
#else
    fprintf(stderr,
        "Uso: %s [-s | send] Archivos...\n"
        "     %s [-r | recv] host:puerto\n\n"
        "Opciones:\n"
        "    -s send    Inicia un servidor y envia archivos.\n"
        "    -r recv    Se conecta a un servidor y recive archivos.\n"
        "    -h         Muestra este mensaje.\n",
        arg0, arg0);
#endif

    exit(EXIT_FAILURE);
}

int
main(int argc, char *argv[])
{
    arg0 = argv[0];

#ifdef N0NC_SPANISH
    qsort(&messagesTable,
          sizeof(messagesTable) / sizeof(struct translation),
          sizeof(struct translation), _sort);
#endif

#ifdef __WIN32__
    WSADATA wsaData;
    int ret;

    ret = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if(ret != 0)
        err(1, "WSAStartup: %i", ret);
#endif

    if(argc == 1) {
        puts(
"N0NC version " VERSION PATCH EXTENSION ", build(" BUILD ")\n"
"Copyright (C) 2018 Neutral0, Jeicraft\n"
"N0NC comes with ABSOLUTELY NO WARRANTY; This is free software, and\n"
"you are welcome to redistribute it under certain condition; for\n"
"details read the source code of this software.\n"
            );
        return 0;
    }

    puts("N0NC version " VERSION PATCH EXTENSION "\n");

    if((strcmp(argv[1], "-s") == 0) || (strcmp(argv[1], "send") == 0)) {
        if(argc < 3)
            err(1, _("missing arguments."));

        sendMode(argv + 2, (argv[1][4] == '+'));
    } else if((strcmp(argv[1], "-r") == 0) || (strcmp(argv[1], "recv") == 0)) {
        if(argc < 3)
            err(1, _("host:port required"));

        recvMode(argv[2]);
    } else if((strcmp(argv[1], "-h") == 0) || (strcmp(argv[1], "help") == 0))
        showHelp();
    else
        err(1, _("invalid mode '%s'"), argv[1]);

#ifdef __WIN32__
    WSACleanup();
#endif

    return 0;
}
