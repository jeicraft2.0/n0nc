#!/usr/bin/env bash

_n0nc_completions()
{
    local cur prev words cword

    _init_completion || return

    UPNP=""
    if n0nc | grep -q UPNP; then
        UPNP="send+"
    fi

    if [[ $prev == "n0nc" ]]; then
        COMPREPLY=($(compgen -W "recv send $UPNP" "$cur"))
    else
        _filedir
        return
    fi
}

complete -F _n0nc_completions n0nc

